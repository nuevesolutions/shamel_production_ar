package com.alrahaprovider.app;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alrahaprovider.hockeyapp.ActivityHockeyApp;

import core.service.ServiceConstant;
import core.socket.SocketHandler;


/**
 * Casperon Technology on 10/1-2/2015.
 */
public class AboutUs extends ActivityHockeyApp {
    private RelativeLayout back;
    private TextView Tv_more_info;
    private SocketHandler socketHandler;
    private WebView myWebView;
    String web_url = "";
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aboutus);
        socketHandler = SocketHandler.getInstance(this);
        back = (RelativeLayout) findViewById(R.id.aboutus_header_back_layout);
        Tv_more_info = (TextView) findViewById(R.id.more_info_baseurl);
        progressBar = (ProgressBar) findViewById(R.id.webView_progressbar);
        myWebView = (WebView) findViewById(R.id.WebView);
        final WebSettings webSettings = myWebView.getSettings();
        Resources res = getResources();
        int fontSize = (int) res.getDimension(R.dimen.textsize);
        webSettings.setDefaultFontSize((int) fontSize);

        web_url= ServiceConstant.Aboutus_Url;
        myWebView.setWebViewClient(new MyWebViewClient());
        myWebView.loadUrl(web_url);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        myWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (progress < 100 && progressBar.getVisibility() == ProgressBar.GONE) {
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                }
                progressBar.setProgress(progress);

                if (progress == 100) {
                    progressBar.setVisibility(ProgressBar.GONE);
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            onBackPressed();
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }


    @Override
    protected void onResume() {
        super.onResume();
    }
    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (Uri.parse(url).getHost().equals("https://www.shamelinc.com/mobile/provider/register")) {
                // This is your web site, so do not override; let the WebView to load the page
                return false;
            }
            // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {

            // this will ignore the Ssl error and will go forward to your site
           /* final AlertDialog.Builder builder = new AlertDialog.Builder(AboutUs.this);
            builder.setMessage("Do you want to continue?");
            builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.proceed();
                }
            });
            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handler.cancel();
                }
            });
            final AlertDialog dialog = builder.create();
            dialog.show();*/
            final Dialog dialog = new Dialog(AboutUs.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            LayoutInflater inflater = (LayoutInflater) AboutUs.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View  view_layout = inflater.inflate(R.layout.your_layout, null, false);
            dialog.setContentView(view_layout);
            TextView txt_cancel = (TextView) view_layout.findViewById(R.id.txt_cancel);
            TextView txt_confirm =(TextView) view_layout.findViewById(R.id.txt_confirm);
            txt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    handler.cancel();
                    dialog.dismiss();
                }
            });
            txt_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handler.proceed();
                    dialog.dismiss();


                }
            });
            final Window window = dialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
            dialog.show();

        }
    }

}
