package com.alrahaprovider.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alrahaprovider.app.R;
import com.alrahaprovider.hockeyapp.FragmentHockeyApp;

/**
 * Created by user88 on 12/10/2015.
 */
public class MyJobs extends FragmentHockeyApp {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.myjobs, container, false);
    }

}
